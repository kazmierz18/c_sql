#pragma once
#include <stdio.h>
#include <stdlib.h>

/**
 * helper functions for int to string conversion
 */
#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

/**
 * Generate string with location that can be parsed by vim
 */
#define LOCATION __FILE__ ":" STR(__LINE__) ": "

/**
 * Prints error message to stderr wit LOCATION used in vim build flow
 */
#define ERROR(msg, ...)                                                        \
  (void)fprintf(stderr, LOCATION "ERROR: " msg __VA_OPT__(, ) __VA_ARGS__)
/**
 * Prints info message to stderr wit LOCATION used in vim build flow
 */
#define INFO(msg, ...)                                                         \
  (void)fprintf(stderr, "INFO: " msg __VA_OPT__(, ) __VA_ARGS__)

/**
 * Prints info message to stderr that program flow is not implemented
 * and stop execution of program
 */
#define TODO()                                                                 \
  do {                                                                         \
    (void)fprintf(stderr, LOCATION "NOT IMPLEMENTED!!\n");                     \
    exit(-1);                                                                  \
  } while (0)

/**
 * Prints error message to stderr that program flow cant handle situation
 * and stop execution of program
 */
#define PANIC()                                                                \
  do {                                                                         \
    (void)fprintf(stderr, LOCATION "at function: %s PANICKED!!\n", __func__);  \
    exit(-1);                                                                  \
  } while (0)
