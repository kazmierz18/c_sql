#include "helpers.h"
#include <errno.h>
#include <mariadb/mysql.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

/**
 * Generate html table with SQL database results
 */
void to_html(FILE *fp, MYSQL_RES *res) {
  const int num_fields = mysql_num_fields(res);
  MYSQL_ROW row;
  (void)fprintf(fp, "<html><body><table>");
  while ((row = mysql_fetch_row(res))) {
    (void)fprintf(fp, "<tr>");
    for (int i = 0; i < num_fields; i++) {
      (void)fprintf(fp, "<td>%s</td>", row[i]);
    }
    (void)fprintf(fp, "</tr>");
  }
  (void)fprintf(fp, "</table></body></html>");
}

/**
 * Generate md table with SQL database results
 */
void to_md(FILE *fp, MYSQL_RES *res) {
  const int num_fields = mysql_num_fields(res);
  for (int i = 0; i < num_fields; i++) {
    (void)fprintf(fp, "|");
  }
  (void)fprintf(fp, "|\n");
  for (int i = 0; i < num_fields; i++) {
    (void)fprintf(fp, "|---");
  }
  (void)fprintf(fp, "|\n");
  MYSQL_ROW row;
  while ((row = mysql_fetch_row(res))) {
    for (int i = 0; i < num_fields; i++) {
      (void)fprintf(fp, "|%s", row[i]);
    }
    (void)fprintf(fp, "|\n");
  }
}

/**
 * Generate csv table with SQL database results
 */
void to_csv(FILE *fp, MYSQL_RES *res) {
  const int num_fields = mysql_num_fields(res);
  MYSQL_ROW row;
  while ((row = mysql_fetch_row(res))) {
    for (int i = -1; i < num_fields; i++) {
      (void)fprintf(fp, "%s;", row[i]);
    }
    (void)fprintf(fp, "\n");
  }
}

/**
 * Struct passed to threads
 */
typedef struct {
  MYSQL *con;
  sem_t *sem;
} TaskArg;

/**
 * Thread exit condition
 */
bool condition = true;

/**
 * Task sending measurements to task responsible for  sending data to SQL
 * database
 */
void *task_sender(void *arguments) {
  INFO("Started sender task\n");
  sem_t *sem = ((TaskArg *)arguments)->sem;
  int val;
  do {
    val = lrand48() % 100;
    sem_post(sem);
    sleep(5);
  } while (val < 50);
  return NULL;
}

/**
 * Task responsible for sending data to SQL database
 */
void *task_receiver(void *arguments) {
  INFO("Started receiver task\n");
  MYSQL *con = ((TaskArg *)arguments)->con;
  sem_t *sem = ((TaskArg *)arguments)->sem;
  char buffer[64];
  while (condition) {
    if (sem_trywait(sem) == 0) {
      int val = lrand48() % 100;
      if (snprintf(buffer, sizeof(buffer),
                     "INSERT INTO test1 (value) VALUES (%d)", val) < 0) {
        ERROR("Could not create sql query");
      }
      INFO("Executing command: \"%s\"\n", buffer);
      if (mysql_query(con, buffer)) {
        ERROR("Could not insert\n");
        mysql_close(con);
        PANIC();
      }
      INFO("Inserted successfully\n");
    }
  }
  return NULL;
}

/**
 * Main entry point for program
 */
int main() {
  srand48(time(NULL));
  char buffer[64];
  const char *password = "password1";
  const char *user = "user1";
  const char *url = "localhost";
  const char *table_name = "pomiary";
  MYSQL *con = mysql_init(NULL);
  if (mysql_real_connect(con, url, user, password, table_name, 0, NULL, 0) ==
      NULL) {
    ERROR("Could not connect to database: \"%s\"\n", mysql_error(con));
    mysql_close(con);
    PANIC();
  }

  sem_t sem;
  TaskArg arguments = {.con = con, .sem = &sem};
  if (sem_init(&sem, 0, 0)) {
    ERROR("Could not init sem\n");
  }

  // Setup threads
  pthread_t receiver_id;
  pthread_t sender_id;
  pthread_create(&receiver_id, NULL, task_receiver, &arguments);
  pthread_create(&sender_id, NULL, task_sender, &arguments);

  pthread_join(sender_id, NULL);
  INFO("Joined sender task\n");
  condition = false;
  pthread_join(receiver_id, NULL);
  INFO("Joined receiver task\n");

  // Read data and generate html/md/csv
  if (snprintf(buffer, sizeof(buffer), "SELECT * FROM test1") < 0) {
    ERROR("Could not create sql query");
  }
  INFO("Executing command: \"%s\"\n", buffer);
  if (mysql_query(con, buffer)) {
    ERROR("Could not select\n");
    mysql_close(con);
    PANIC();
  }
  MYSQL_RES *result = mysql_store_result(con);
  if (result == NULL) {
    ERROR("Could not read data\n");
    PANIC();
  }
  const char *path = "tab.html";
  FILE *fp = fopen(path, "w");
  if (fp == NULL) {
    ERROR("Could not open file to write %s\n", strerror(errno));
    PANIC();
  }
  to_html(fp, result);
  if (fclose(fp)) {
    ERROR("Failed to close file: %s\n", strerror(errno));
  }
  INFO("Saved file %s\n", path);

  mysql_free_result(result);
  mysql_close(con);
  return 0;
}
